var katexConfig = {
	delimiters:
		[
			{ left: "$$", right: "$$", display: true },
			{ left: "$", right: "$", display: false }
		],
	throwOnError: false
};

function initialize() {
	// convert markdown to html with Showdown.js
	var source = document.getElementById("markdownContainer").innerText;
	var converter = new showdown.Converter();
	var result = converter.makeHtml(source);
	document.getElementById("main").innerHTML = result;

	// rendering Math with KaTex
	renderMathInElement(document.getElementById("main"), katexConfig);

	// rendering code highlight with highlight.js
	hljs.highlightAll();
}

