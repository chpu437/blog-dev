var katexConfig = {
	delimiters:
		[
			{ left: "$$", right: "$$", display: true },
			{ left: "$", right: "$", display: false }
		],
	throwOnError: false
};

function showdown_convert() {
	var source = document.getElementById("markdownContainer").innerText;
	var converter = new showdown.Converter();
	converter.setOption("strikethrough", "true");
	converter.setOption("tables", "true");
	converter.setOption("ghCompatibleHeaderId", "true");
	converter.setOption("simpleLineBreaks", "true");
	converter.setOption("parseImgDimensions", "true");
	converter.setOption("literalMidWordUnderscores", "true"); // 不加这句话小心公式歇菜
	converter.setOption("tasklists", "true");
	converter.setOption("ghMentions", "true");
	converter.setOption("headerLevelStart", 2);
	var result = converter.makeHtml(source);
	document.getElementById("main").innerHTML = result;
}

function initialize() {
	// convert markdown to html with Showdown.js
	showdown_convert();

	// rendering Math with KaTex
	renderMathInElement(document.getElementById("main"), katexConfig);

	// rendering // code highlight with highlight.js
	hljs.highlightAll();
}